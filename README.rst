====================
proteus-script-frame
====================

`proteus-script-frame` is a gereneral usable frame for proteus scripts.
It handles the initial steps to get connected to Tryton with the help
of command line options. You can extend it with your own code.

Features
--------
* A simple comand line interface
* Find trytond.conf either via config paramter or from environment
* Connect to Tryton database (sqlite or postgresql)
* Disable trytond debug logging
* Handle serveral error cases on startup

Usage
-----

::

  usage: proteus-script-frame.py [-h] [-c FILE] -d DATABASE [-v] [-n]

  optional arguments:
  -h, --help            show this help message and exit
  -c FILE, --config FILE
                        tryton config file (if not used, TRYTOND_CONFIG
                        environment must be set)
  -d DATABASE, --database DATABASE
                        name of database
  -v, --verbose         enable verbose mode (-vv for more details)
  -n, --dry-run         don't save changes

